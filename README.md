# nikas - 2020-03-11

## Installation

### Via yarn
* Prerequisites:
  * Node 12.x - can use `nvm use` to automatically setup
  * yarn
* API
  * `yarn` - install required dependencies
  * `yarn migrate` - run migrations and prepare database for use
  * `yarn build` - generate production ready build
  * `yarn dev` - start the API in development mode, accessible at http://localhost:4111
  * `yarn test` - start tests in watch mode
  * `yarn lint` - run eslint
* APP
  * `yarn` - install required dependencies
  * `yarn build` - generate production ready build
  * `yarn dev` - start the app in development mode, accessible at http://localhost:3000
    * **Note:** expects the API to be running under http://localhost:4111
  * `yarn test` - start tests in watch mode
  * `yarn lint` - run eslint

#### Typical first run flow (2 terminals):
* Under /api
  1. `yarn`
  2. `yarn migrate`
  3. `yarn dev`
* Under /app
  1. `yarn`
  2. `yarn dev`

### Via docker-compose
* Prerequisites:
  * Docker
* `docker-compose up` - prepare and run a simple dev environment in watch mode

## Security

### Addressed:
* App: added CSP
  * **Note:** create-react-app includes an inline script on build time, chose to add SHA for it manually (see improvements)
* App: reviewed code for potential XSS vulnerabilities, found none
  * React automatically escapes simple renders
  * No `dangerouslySetInnerHTML` used
  * No prop spreading from user data
  * No style spreading from user data
  * No href with user input
* App: no `target="_blank"` links, so `rel=”noopener noreferrer”` unnecessary
* App: no third-party scripts from CDNs used, so subresource integrity attributes not necessary either
* App: `yarn audit`:
  * [acorn](https://www.npmjs.com/advisories/1488) - should not be relevant, but added resolution just in case
* API: `yarn audit` reports no vulnerabilities
* API: file uploads larger than limit are automatically dropped with 413 to prevent overloading the API
* API: CORS header is minimal and only allows requests from required domains (not wildcard)
* API: response structure is well-defined to prevent accidental data leaks (i.e. no spread or returning of Sequelize instance)
* API: files are being uploaded to a database, rather than messing about with the file system
* API: using Sequelize to prevent SQL injection
* API: CSRF should not be an issue since using neither basic auth nor cookies (nor any auth at all, in fact)

### TODO:
* App: when serving from CDN, use the `X-Frame-Options: DENY` header to prevent clickjacking
* App: when serving from CDN, use HSTS header to ensure site can only be accessed using HTTPS (also add to https://hstspreload.org/)
* API: remove X-Powered-By header to not disclose server stack
* API: file upload URL should expire after a short period of time (i.e. a few minutes)
* API: improve malicious attachment detection
  * basic mime-type detection added, but easy to fool
  * compress images losslessly? Could be done by CDN in a real app; did not want to mess with setting up an image minification library. Would remove weird content (i.e. html inside jpg)
  * use something like [mmmagic](https://www.npmjs.com/package/mmmagic) to validate image?
  * investigate other libraries specifically for detecting shady file uploads
* Both: when deploying, use HTTPS
* Both: Authentication and authorization

## Improvements

### General
* Add testing and linting to docker-compose flow
* Both API and app extend from a common eslint base config

### App
* Firefox: for a file that's too big, the fetch request seems to drop completely rather than returning a 413. Might be an issue with the `express-file-upload` library or Firefox, would need to investigate. Works fine on Chrome.
* CSP hashes for inline scripts should be generated on build time, API hosts based on environments
* PostCSS or SCSS could be added to add some syntactic sugar and make managing variables easier
* We might want to hide extensions for file names
* Add pagination
* Highlight search term in results
* Nicer loading experience (i.e. animation, blurred previous results underneath)
* Should truncate document name rather than wrapping
* Add support for viewing/downloading documents
  * Has security implications (i.e. need to avoid XSS), so skipped since it was not required
* create-react-app uses `dependencies` by default for everything, would need to split some into `devDependencies`. Tricky because it depends on build process
* Search can technically return stale results, but since we're on localhost here and the solution is a bit awkward, decided to skip for now
  * Could solve via something like described [here](https://reactjs.org/docs/hooks-faq.html#is-it-safe-to-omit-functions-from-the-list-of-dependencies)

### API
* Add support for TypeScript migration files
* Add endpoint versioning
* Add test builders
* Separate out routes from controllers (ideally express should just thinly wrap some internal business logic)
* Paginate documents endpoint
* Add support for downloading the file
* Use a proper database rather than SQLite (and connect to it rather than embed)
* Maybe use paranoid deletion rather than actual deletion? Depends on data retention policy

## Libraries

### App

The app is made using a standard TypeScript create-react-app stack. Other than that:
* [classnames](https://www.npmjs.com/package/classnames) - greatly simplifies className construction in React (i.e. conditional classes)
* [use-debounce](https://www.npmjs.com/package/use-debounce) - for debouncing the search input (to prevent a new request after each keystroke)

### API
The app is made using a standard express stack. Other than that:
* [express-async-errors](https://www.npmjs.com/package/express-async-errors) - adds proper async/await handling to express middleware. Otherwise, thrown errors are not correctly handled
* [express-fileupload](https://www.npmjs.com/package/express-fileupload) - handles multipart file uploads. Used this instead of asking for a Base64 encoded file from the client side because encoding large files on the client-side can be costly and it would have to mean increasing request size
* [sequelize](https://www.npmjs.com/package/sequelize) - my go-to default ORM. Provides security and convenience. Could be replaced by another similar library, but still would like to have some kind of layer between business logic and SQL
* [sqlite3](https://www.npmjs.com/package/sqlite3) - simple embedded database, did not want to complicate the app
* [umzug](https://www.npmjs.com/package/umzug) - migration utility, allows greater flexibility and consistency between production use and tests

## API
All API endpoints accept and return JSON, unless otherwise specified.

### GET /documents
Return all documents and metadata

#### Query parameters:
* `q` - optional, filter documents by name (substring)

#### Returns:
```ts
{
    data: Array<{
        id: string; // document id
        name: string; // document name
        sizeInBytes: number | undefined; // document file size in bytes, can be undefined if file has not been uploaded yet
    }>;
    totalFileSize: number; // sum of all file sizes of documents in data
}
```

### POST /documents
Create a new document and prepare for file upload

#### Body:
```ts
{
    name: string; // document name
}
```

#### Returns:
```ts
{
    id: string; // document id
    name: string; // document name
    uploadUrl: string; // url to which to POST the file to (see below)
}
```

### POST ${uploadUrl}
Upload the file to finish creating document

**Note:** File size limit is 10 MB

**Note:** currently implemented as `POST /documents/:id/upload`, but this is an implementation detail and may change

#### Body as `multipart/form-data`:
```ts
{
    file: File; // document file, max 10 MB
}
```

#### Returns:
```ts
{
    id: string; // document id
    name: string; // document name
    sizeInByes: number; // document file size in bytes
}
```

### DELETE /documents/:id
Delete a document with the specified id

#### Path parameters:
* `:id` - required, the id of the document to delete

---

## Other notes

* Please see https://gitlab.com/nikaspran/nikas-2020-02-25 for an example CI/CD setup
