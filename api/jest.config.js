module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  coveragePathIgnorePatterns: [
    "<rootDir>/node_modules",
    "<rootDir>/db/migrations",
    "<rootDir>/src/scripts",
    "<rootDir>/src/test",
  ],
};
