import Umzug from 'umzug';
import sequelize from '../common/sequelize';

function initUmzug({ silent }: { silent?: boolean } = {}) {
  return new Umzug({
    logging: !silent && console.log, // eslint-disable-line no-console
    storage: 'sequelize',
    storageOptions: {
      sequelize,
      tableName: 'sequelize_migrations',
    },
    migrations: {
      params: [
        sequelize.getQueryInterface(),
        sequelize.constructor,
        () => {
          throw new Error('Migration tried to use old style "done" callback. Please use promise instead');
        },
      ],
      path: 'db/migrations',
    },
  });
}

export async function up(...args: Parameters<typeof initUmzug>) {
  const umzug = initUmzug(...args);
  return umzug.up();
}

export async function down(...args: Parameters<typeof initUmzug>) {
  const umzug = initUmzug(...args);
  return umzug.down();
}

if (require.main === module) { // run via `node migrate`
  const isUndo = process.argv.includes('--undo');

  const run = isUndo ? down : up;
  run()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error); // eslint-disable-line no-console
      process.exit(1);
    });
}
