import Sequelize, { Model } from 'sequelize';
import sequelize from '../common/sequelize';

export default class Document extends Model {
  public id: string;

  public name: string;

  public sizeInBytes: number;

  public data: Buffer;
}

Document.init({
  id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
  name: { type: Sequelize.STRING, allowNull: false },
  sizeInBytes: { type: Sequelize.INTEGER },
  data: { type: Sequelize.BLOB },
}, {
  sequelize,
  tableName: 'documents',
  paranoid: false,
});
