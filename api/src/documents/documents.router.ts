import { Op } from 'sequelize';
import express, { Request } from 'express';
import fileUpload, { UploadedFile } from 'express-fileupload';
import Document from './document.model';
import config from '../common/config';

const router = express.Router();

const MEGABYTE = 1000000;

function maybeAddQuery(req: Request) {
  const { q } = req.query;
  if (!q) {
    return {};
  }

  return {
    name: { [Op.substring]: q },
  };
}

router.get('/documents', async (req, res) => {
  const documents = await Document.findAll({
    where: {
      ...maybeAddQuery(req),
    },
    attributes: ['id', 'name', 'sizeInBytes'],
  });

  // this would be a separate query if using pagination, but simple sum will do for now
  const totalFileSize = documents.reduce((sum, document) => sum + document.sizeInBytes, 0);

  res.status(200).send({
    data: documents,
    totalFileSize,
  });
});

router.post('/documents', async (req, res) => {
  const created = await Document.create({
    name: req.body.name,
  });

  res.status(201).send({
    id: created.id,
    name: created.name,
    uploadUrl: `${config.assetBaseUrl}/documents/${created.id}/upload`, // this would in reality be S3 or similar
  });
});

router.post('/documents/:id/upload', fileUpload({
  limits: { fileSize: 10 * MEGABYTE },
  safeFileNames: true,
  preserveExtension: true,
  abortOnLimit: true,
}), async (req, res) => {
  const file = req.files && req.files.file as UploadedFile;

  if (!file) {
    res.status(400).send({ error: 'File missing' });
    return;
  }

  if (!['image/jpeg', 'image/png'].includes(file.mimetype)) {
    res.status(400).send({ error: 'Unsupported file type, only jpg and png accepted' });
    return;
  }

  const document = await Document.findByPk(req.params.id);
  if (!document || document.data) {
    res.sendStatus(404);
    return;
  }

  await document.update({
    sizeInBytes: file.size,
    data: file.data,
  });

  res.send({
    id: document.id,
    name: document.name,
    sizeInBytes: document.sizeInBytes,
  });
});

router.delete('/documents/:id', async (req, res) => {
  const document = await Document.findByPk(req.params.id);
  if (document) {
    await document.destroy();
  }
  res.sendStatus(204); // do not reveal whether record existed or not, could return 404 if security model allows
});

export default router;
