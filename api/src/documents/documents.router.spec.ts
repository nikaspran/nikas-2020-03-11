import { request, prepareDatabase } from '../test/testUtils';
import Document from './document.model';
import config from '../common/config';

const {
  any, anything, arrayContaining, objectContaining,
} = expect;

describe('documents.router', () => {
  beforeAll(prepareDatabase);
  beforeEach(async () => {
    await Document.truncate(); // TODO: this does not scale to multiple models and associations, but will do for now
    // in a real app, use something like a builder pattern to automatically remove built models or full blackbox testing
  });

  describe('GET /documents', () => {
    it('should return all stored documents', async () => {
      const document1 = await Document.create({ name: 'someDocument' });
      const document2 = await Document.create({ name: 'anotherDocument' });

      const response = await request.get('/documents')
        .expect(200);

      expect(response.body.data).toEqual(arrayContaining([
        objectContaining({ id: document1.id }),
        objectContaining({ id: document2.id }),
      ]));
      expect(response.body.data).toHaveLength(2);
    });

    it('should return the total size in a header', async () => {
      await Document.create({ name: 'someDocument', sizeInBytes: 100 });
      await Document.create({ name: 'anotherDocument', sizeInBytes: 23 });

      const response = await request.get('/documents')
        .expect(200);

      expect(response.body).toMatchObject({
        totalFileSize: 123,
      });
    });

    describe('?q', () => {
      it('should return documents if their name matches the query', async () => {
        const match1 = await Document.create({ name: 'some document' });
        const match2 = await Document.create({ name: 'middle some document' });
        await Document.create({ name: 'not a match' });

        const response = await request.get('/documents?q=some')
          .expect(200);

        expect(response.body.data).toEqual(arrayContaining([
          objectContaining({ id: match1.id }),
          objectContaining({ id: match2.id }),
        ]));
        expect(response.body.data).toHaveLength(2);
      });
    });
  });

  describe('POST /documents', () => {
    it('should create a new document', async () => {
      await request.post('/documents')
        .send({ name: 'someDocument' })
        .expect(201);

      expect(await Document.findAll()).toEqual(arrayContaining([
        objectContaining({ name: 'someDocument' }),
      ]));
      expect(await Document.findAll()).toHaveLength(1);
    });

    it('should return the created document', async () => {
      const response = await request.post('/documents')
        .send({ name: 'someDocument' })
        .expect(201);

      expect(response.body).toEqual(objectContaining({ id: any(String), name: 'someDocument' }));
    });

    it('should return an uploadUrl for file uploading', async () => {
      const response = await request.post('/documents')
        .send({ name: 'someDocument' })
        .expect(201);

      expect(response.body).toEqual(objectContaining({
        uploadUrl: `${config.assetBaseUrl}/documents/${response.body.id}/upload`,
      }));
    });
  });

  describe('POST /documents/:id/upload', () => {
    it('should update the document with the file', async () => {
      const someDocument = await Document.create({ name: 'someDocument' });

      await request.post(`/documents/${someDocument.id}/upload`)
        .attach('file', Buffer.alloc(100), { filename: 'image.jpg' })
        .expect(200);

      await someDocument.reload();

      expect(someDocument).toEqual(objectContaining({
        data: any(Buffer),
        sizeInBytes: 100,
      }));
    });

    it('should not allow files larger than 10 MB', async () => {
      const someDocument = await Document.create({ name: 'someDocument' });

      await request.post(`/documents/${someDocument.id}/upload`)
        .attach('file', Buffer.alloc(1 + 10 * 1000 * 1000), { filename: 'image.jpg' })
        .expect(413);

      await someDocument.reload();

      expect(someDocument).toEqual(objectContaining({
        data: null,
        sizeInBytes: null,
      }));
    });

    it('should work with weird characters in the file name', async () => {
      const someDocument = await Document.create({ name: 'someDocument' });

      await request.post(`/documents/${someDocument.id}/upload`)
        .attach('file', Buffer.alloc(100), { filename: '/usr/local/_test.jpg' })
        .expect(200);

      await someDocument.reload();

      expect(someDocument).toEqual(objectContaining({ name: 'someDocument' }));
    });

    it('should not allow files with unsupported mime-types', async () => {
      const someDocument = await Document.create({ name: 'someDocument' });

      await request.post(`/documents/${someDocument.id}/upload`)
        .attach('file', Buffer.alloc(100), { filename: '/usr/local/_test.jpg', contentType: 'application/pdf' })
        .expect(400);
    });

    it('should respond with the full document apart from data', async () => {
      const someDocument = await Document.create({ name: 'someDocument' });

      const response = await request.post(`/documents/${someDocument.id}/upload`)
        .attach('file', Buffer.alloc(123), { filename: 'image.png' })
        .expect(200);

      expect(response.body).toMatchObject({
        id: someDocument.id,
        name: 'someDocument',
        sizeInBytes: 123,
      });
      expect(response.body).not.toMatchObject({ data: anything() });
    });

    it('should only work once', async () => {
      const someDocument = await Document.create({ name: 'someDocument' });

      await request.post(`/documents/${someDocument.id}/upload`)
        .attach('file', Buffer.alloc(123), { filename: 'image.jpg' })
        .expect(200);

      await request.post(`/documents/${someDocument.id}/upload`)
        .attach('file', Buffer.alloc(321), { filename: 'image.jpg' })
        .expect(404);

      await someDocument.reload();
      expect(someDocument.sizeInBytes).toEqual(123);
    });

    it('should return 404 for a missing document', async () => {
      await request.post('/documents/123-321/upload')
        .attach('file', Buffer.alloc(123), { filename: 'image.jpg' })
        .expect(404);
    });

    it('should return 400 for a missing file attachment', async () => {
      await request.post('/documents/123-321/upload')
        .expect(400);
    });
  });

  describe('DELETE /documents', () => {
    it('should delete document', async () => {
      const deletedDocument = await Document.create({ name: 'deletedDocument' });
      const undeletedDocument = await Document.create({ name: 'undeletedDocument' });

      await request.delete(`/documents/${deletedDocument.id}`)
        .expect(204);

      expect(await Document.findAll()).toEqual(arrayContaining([
        objectContaining({ id: undeletedDocument.id }),
      ]));
      expect(await Document.findAll()).toHaveLength(1);
    });

    it('should do nothing but return 204 if document is missing', async () => {
      const oldDocumentCount = await Document.count();

      await request.delete('/documents/missing-id')
        .expect(204);

      expect(await Document.count()).toEqual(oldDocumentCount);
    });
  });
});
