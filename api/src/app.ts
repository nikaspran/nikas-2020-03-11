import express, { ErrorRequestHandler } from 'express';
import cors from 'cors';
import 'express-async-errors';
import bodyParser from 'body-parser';
import documentsRouter from './documents/documents.router';
import config from './common/config';

const app = express();

app.use(cors({
  origin: [
    ...config.allowedCorsOrigins,
  ],
  allowedHeaders: [
    'Content-Type',
  ].join(','),
}));
app.use(bodyParser.json());
app.use(documentsRouter);

app.use(function errorHandler(error, req, res, next) {
  if (res.headersSent) {
    next(error);
    return;
  }

  console.error(error); // eslint-disable-line no-console
  res.status(500);
  res.send({ error: 'Internal server error' });
} as ErrorRequestHandler);

export default app;
