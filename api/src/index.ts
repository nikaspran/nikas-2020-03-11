import app from './app';
import config from './common/config';

app.listen(config.port, () => {
  console.log(`API listening on port ${config.port}`); // eslint-disable-line no-console
});
