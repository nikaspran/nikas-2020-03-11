import supertest from 'supertest';
import app from '../app';
import * as migrate from '../scripts/migrate';

export const request = supertest(app);

export async function prepareDatabase() {
  // ideally this would be run once in globalSetup,
  // since globalSetup has a separate process, sqlite :memory: does persist between suites
  try {
    await migrate.up({ silent: true });
  } catch (error) {
    console.error('Could not run migrations', error); // eslint-disable-line no-console
    throw error;
  }
}
