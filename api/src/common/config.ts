const devConfig = {
  port: '4111',
  assetBaseUrl: 'http://localhost:4111',
  allowedCorsOrigins: [
    'http://localhost:3000',
  ],
  sequelize: {
    dialect: 'sqlite',
    storage: './db/database.sqlite',
  },
};

const testConfig = {
  port: '4111',
  assetBaseUrl: 'http://localhost:4111',
  allowedCorsOrigins: [] as string[],
  sequelize: {
    dialect: 'sqlite',
    storage: ':memory:',
  },
};

export default {
  ...(process.env.NODE_ENV === 'test' ? testConfig : devConfig),
};
