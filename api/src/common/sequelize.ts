import { Sequelize, Options } from 'sequelize';
import config from './config';

export default new Sequelize({
  logging: false,
  ...config.sequelize as Options,
});
