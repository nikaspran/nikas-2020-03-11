const DOCUMENTS_TABLE = 'documents';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn(DOCUMENTS_TABLE, 'sizeInBytes', { type: Sequelize.INTEGER });
    await queryInterface.addColumn(DOCUMENTS_TABLE, 'data', { type: Sequelize.BLOB });
  },
  async down(queryInterface) {
    await queryInterface.removeColumn(DOCUMENTS_TABLE, 'data');
    await queryInterface.removeColumn(DOCUMENTS_TABLE, 'sizeInBytes');
  },
};
