module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.createTable('documents', {
      id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
      name: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE },
      updatedAt: { type: Sequelize.DATE },
    })
  },
  async down(queryInterface) {
    return queryInterface.dropTable('documents');
  },
};
