module.exports = {
  "root": true,
  "parser": "@typescript-eslint/parser",
  "plugins": [
    "@typescript-eslint"
  ],
  "extends": [
    "airbnb-base",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  "rules": {
    "max-len": ["error", { "code": 120 }],
    "newline-per-chained-call": ["off"],
    "import/extensions": "off",
    "import/no-extraneous-dependencies": ["error", {
      "devDependencies": [
        "src/test/**/*",
        "**/*{.,_}{test,spec}.{ts,tsx,js,jsx}"
      ],
      "optionalDependencies": false
    }],
  },
  "overrides": [
    {
      "files": ["*.ts", "*.tsx"],
      "rules": {
        "func-call-spacing": "off",
        "no-spaced-func": "off",
        "import/no-unresolved": "off",
        "import/first": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
      }
    }
  ],

}
