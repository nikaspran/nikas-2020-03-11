import React from 'react';
import { render } from '@testing-library/react';
import FileSize from './FileSize';

describe('<FileSize />', () => {
  it('should render the precise byte amount if under 1KB', () => {
    const { getByText } = render(<FileSize bytes={453} />);
    expect(getByText('453 B')).toBeInTheDocument();
  });

  it('should render a rounded kilobyte amount if above 1KB', () => {
    const { getByText } = render(<FileSize bytes={1453} />);
    expect(getByText('1 KB')).toBeInTheDocument();
  });

  it('should render nothing if bytes is undefined', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const { container } = render(<FileSize bytes={undefined as any} />);
    expect(container.firstChild).toBeFalsy();
  });
});
