import React from 'react';

export default function FileSize({
  bytes,
}: {
  bytes: number;
}) {
  if (!bytes) {
    return null;
  }

  if (bytes < 1000) {
    return <>{bytes} B</>;
  }

  return <>{Math.round(bytes / 1000)} KB</>;
}
