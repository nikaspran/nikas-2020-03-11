import React, { useState } from 'react';
import classNames from 'classnames';
import styles from './DocumentCard.module.css';
import Button from '../../common/components/Button';
import { Document } from '../services/documentService';
import FileSize from './FileSize';

export default function DocumentCard({
  className,
  document,
  onDelete,
}: {
  className?: string;
  document: Document;
  onDelete?: (document: Document) => Promise<unknown>;
}) {
  const [deleting, setDeleting] = useState(false);

  async function notifyAboutDeletion() {
    if (!onDelete) {
      return;
    }

    setDeleting(true);
    try {
      await onDelete(document);
    } catch { // if promise resolves, this node will be gone, no need to update
      setDeleting(false);
    }
  }

  return (
    <div className={classNames(styles.card, className)}>
      <div className={styles.name}>{document.name}</div>

      <div className={styles.controls}>
        <div>
          <FileSize bytes={document.sizeInBytes} />
        </div>
        {onDelete && (
          <Button type="button" className={styles.deleteButton} onClick={notifyAboutDeletion} disabled={deleting}>
            {deleting ? 'Deleting...' : 'Delete'}
          </Button>
        )}
      </div>
    </div>
  );
}
