import { when } from 'jest-when';
import { fetchDocuments, deleteDocument, createDocument } from './documentService';

const { objectContaining, any } = expect;

const jsonResult = (result: {}, { status = 200, headers = {} } = {}) => ({
  json: async () => result,
  status,
  headers: new Headers(headers),
});

const textResult = (result: string, { status = 200, headers = {} } = {}) => ({
  text: async () => result,
  status,
  headers: new Headers(headers),
});

describe('documentService', () => {
  let mockFetch: jest.Mock;

  beforeEach(() => {
    window.fetch = jest.fn();
    mockFetch = window.fetch as jest.Mock;
  });

  describe('fetchDocuments()', () => {
    it('should fetch documents from the API', async () => {
      when(mockFetch)
        .calledWith('http://localhost:4111/documents', objectContaining({
          method: 'GET',
        }))
        .mockResolvedValue(jsonResult({
          data: [
            { id: 'someId', name: 'someName' },
            { id: 'anotherId', name: 'anotherName' },
          ],
          totalFileSize: 123,
        }));

      const response = await fetchDocuments();
      expect(response).toEqual({
        data: [
          { id: 'someId', name: 'someName' },
          { id: 'anotherId', name: 'anotherName' },
        ],
        totalFileSize: 123,
      });
    });

    it('should pass along the query if given', async () => {
      when(mockFetch)
        .calledWith('http://localhost:4111/documents?q=some+query', objectContaining({
          method: 'GET',
        }))
        .mockResolvedValue(jsonResult({
          data: [{ id: 'someId', name: 'some query' }],
          totalFileSize: 123,
        }));

      const response = await fetchDocuments({ query: 'some query' });
      expect(response).toEqual({
        data: [{ id: 'someId', name: 'some query' }],
        totalFileSize: 123,
      });
    });

    it('should format and rethrow an error if one occurs', async () => {
      when(mockFetch)
        .calledWith('http://localhost:4111/documents', objectContaining({
          method: 'GET',
        }))
        .mockResolvedValue(jsonResult({
          error: 'Some Error',
        }, { status: 400, headers: { 'Content-Type': 'application/json' } }));

      await expect(fetchDocuments()).rejects.toThrow('[400 Error] Some Error');
    });
  });

  describe('createDocument()', () => {
    it('should create a document and then upload the file to the returned endpoint', async () => {
      when(mockFetch)
        .calledWith('http://localhost:4111/documents', objectContaining({
          method: 'POST',
          body: JSON.stringify({ name: 'dummyFile.png' }),
        }))
        .mockResolvedValue(jsonResult({ uploadUrl: 'http://localhost:4111/someUploadUrl' }));

      when(mockFetch)
        .calledWith('http://localhost:4111/someUploadUrl', objectContaining({
          method: 'POST',
          body: any(FormData),
        }))
        .mockResolvedValue(jsonResult({ id: 'result-id' }));

      const dummyFile = new File([], 'dummyFile.png');
      const response = await createDocument(dummyFile);

      expect(response).toEqual({ id: 'result-id' });
    });

    it('should format and rethrow an error if one occurs', async () => {
      when(mockFetch)
        .calledWith('http://localhost:4111/documents', objectContaining({
          method: 'POST',
          body: JSON.stringify({ name: 'dummyFile.png' }),
        }))
        .mockResolvedValue(textResult('Some Error', { status: 400, headers: { 'Content-Type': 'text/plain' } }));

      const dummyFile = new File([], 'dummyFile.png');

      await expect(createDocument(dummyFile)).rejects.toThrow('[400 Error] Some Error');
    });
  });

  describe('deleteDocument()', () => {
    it('should delete a document from the API', async () => {
      when(mockFetch)
        .calledWith('http://localhost:4111/documents/someId', objectContaining({
          method: 'DELETE',
        }))
        .mockResolvedValue(jsonResult({
          data: [{ id: 'someId', name: 'some query' }],
          totalFileSize: 123,
        }, { status: 204 }));

      await deleteDocument({ id: 'someId' });
    });

    it('should format and rethrow an error if one occurs', async () => {
      when(mockFetch)
        .calledWith('http://localhost:4111/documents/someId', objectContaining({
          method: 'DELETE',
        }))
        .mockResolvedValue(jsonResult({
          error: 'Some Error',
        }, { status: 400, headers: { 'Content-Type': 'application/json' } }));

      await expect(deleteDocument({ id: 'someId' })).rejects.toThrow('[400 Error] Some Error');
    });
  });
});
