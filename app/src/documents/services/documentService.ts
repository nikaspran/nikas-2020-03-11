const API_BASE = 'http://localhost:4111';

export interface Document {
  id: string;
  name: string;
  sizeInBytes: number;
}

async function parseBodyByContentType(response: Response) {
  const contentType = response.headers.get('Content-Type') || '';
  if (contentType.includes('application/json')) {
    return response.json();
  }
  return response.text();
}

type Method = 'GET' | 'POST' | 'DELETE';

async function rawRequest(url: string, options: RequestInit) {
  const response = await fetch(url, options);

  if (response.status >= 400) {
    const errorBody = await parseBodyByContentType(response);
    throw new Error(`[${response.status} Error] ${errorBody.error || errorBody}`);
  }

  if (response.status === 204) {
    return undefined;
  }

  return response.json();
}

async function jsonRequest(path: string, {
  method = 'GET',
  body,
}: {
  method?: Method;
  body?: {};
} = {}) {
  return rawRequest(`${API_BASE}${path}`, {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  });
}

const toSearch = (params: {}) => new URLSearchParams(params).toString();

export async function fetchDocuments({ query }: { query?: string } = {}) {
  const queryString = query ? `?${toSearch({ q: query })}` : '';
  const response = await jsonRequest(`/documents${queryString}`);
  return response as { data: Document[]; totalFileSize: number };
}

export async function createDocument(file: File) {
  const partialDocument = await jsonRequest('/documents', {
    method: 'POST',
    body: {
      name: file.name,
    },
  });

  const formData = new FormData();
  formData.append('file', file);

  const response = await rawRequest(partialDocument.uploadUrl, {
    method: 'POST',
    body: formData,
  });
  return response;
}

export async function deleteDocument({ id }: { id: string }) {
  await jsonRequest(`/documents/${id}`, { method: 'DELETE' });
}
