import React from 'react';
import { when } from 'jest-when';
import {
  render, waitForDomChange, act, fireEvent,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Documents from './Documents';
import * as documentService from './services/documentService';

const mockDocumentService = documentService as { [K in keyof typeof documentService]: jest.Mock };

const { objectContaining } = expect;

jest.mock('./services/documentService', () => ({
  fetchDocuments: jest.fn(),
  createDocument: jest.fn(),
  deleteDocument: jest.fn(),
}));

describe('<Documents />', () => {
  beforeEach(() => {
    jest.useFakeTimers();

    mockDocumentService.fetchDocuments.mockReset();
    mockDocumentService.fetchDocuments.mockResolvedValue({ data: [] });

    mockDocumentService.createDocument.mockReset();
    mockDocumentService.deleteDocument.mockReset();
  });

  function renderComponent() {
    const utils = render(
      <Documents />,
    );

    return {
      ...utils,
      elements: {
        searchInput: () => utils.getByLabelText('Search documents') as HTMLInputElement,
        uploadButton: () => utils.getByLabelText('Upload document') as HTMLInputElement,
      },
    };
  }

  async function waitForDebounce() {
    await act(async () => {
      jest.runOnlyPendingTimers();
    });
  }

  it('should render a list of documents', async () => {
    mockDocumentService.fetchDocuments.mockResolvedValue({
      data: [
        { id: '1', name: 'Document 1' },
        { id: '2', name: 'Document 2' },
      ],
    });

    const { getByText } = renderComponent();

    await waitForDomChange();

    expect(getByText('Document 1')).toBeInTheDocument();
    expect(getByText('Document 2')).toBeInTheDocument();
  });

  it('should display the total file size of results', async () => {
    mockDocumentService.fetchDocuments.mockResolvedValue({
      data: [],
      totalFileSize: 4670,
    });

    const { getByText } = renderComponent();

    await waitForDomChange();

    expect(getByText('Total size: 5 KB')).toBeInTheDocument();
  });

  it('should display number of documents', async () => {
    mockDocumentService.fetchDocuments.mockResolvedValue({
      data: [
        { id: '1', name: 'Document 1' },
        { id: '2', name: 'Document 2' },
      ],
    });

    const { getByText } = renderComponent();

    await waitForDomChange();

    expect(getByText('2 documents')).toBeInTheDocument();
  });

  it('should display a matching list of documents on entering some search term', async () => {
    when(mockDocumentService.fetchDocuments)
      .mockResolvedValue({ data: [{ id: '2', name: 'Document 1' }] })
      .calledWith({ query: 'some query' }).mockResolvedValue({ data: [{ id: '1', name: 'some query document' }] });

    const { getByText, elements } = renderComponent();

    await waitForDomChange();

    await userEvent.type(elements.searchInput(), 'some query');
    await waitForDebounce();

    expect(getByText('some query document')).toBeInTheDocument();
    expect(getByText('1 document'));
  });

  it('should allow clearing the search term', async () => {
    when(mockDocumentService.fetchDocuments)
      .calledWith({ query: '' }).mockResolvedValue({ data: [{ id: '2', name: 'empty query document' }] })
      .calledWith({ query: 'some query' }).mockResolvedValue({ data: [{ id: '1', name: 'some query document' }] });

    const { getByText, elements } = renderComponent();

    await waitForDomChange();

    await userEvent.type(elements.searchInput(), 'some query');
    await waitForDebounce();

    fireEvent.change(elements.searchInput(), { target: { value: '' } });
    await waitForDebounce();

    expect(getByText('empty query document')).toBeInTheDocument();
  });

  it('should allow uploading a document', async () => {
    const { getByText, elements } = renderComponent();

    await waitForDomChange();

    mockDocumentService.createDocument.mockResolvedValue(undefined);
    mockDocumentService.fetchDocuments.mockResolvedValue({ data: [{ id: '2', name: 'uploaded document' }] });

    const dummyFile = new File([], 'someFile.png');
    fireEvent.change(elements.uploadButton(), { target: { files: [dummyFile] } });

    await waitForDomChange();

    expect(mockDocumentService.createDocument).toHaveBeenCalledWith(dummyFile);
    expect(getByText('uploaded document')).toBeInTheDocument();
  });

  it('should allow deleting a document', async () => {
    mockDocumentService.fetchDocuments.mockResolvedValue({
      data: [
        { id: '1', name: 'Document 1' },
        { id: '2', name: 'Document 2' },
      ],
    });

    const { queryByText, getAllByText } = renderComponent();

    await waitForDomChange();

    mockDocumentService.deleteDocument.mockResolvedValue(undefined);
    mockDocumentService.fetchDocuments.mockResolvedValue({ data: [{ id: '2', name: 'Document 2' }] });

    userEvent.click(getAllByText('Delete')[0]);

    await waitForDomChange();

    expect(mockDocumentService.deleteDocument).toHaveBeenCalledWith(objectContaining({ id: '1' }));
    expect(queryByText('Document 1')).not.toBeInTheDocument();
  });

  it('should display an error notification if fetching documents fails', async () => {
    mockDocumentService.fetchDocuments.mockRejectedValue(new Error());

    const { getByText } = renderComponent();

    await waitForDomChange();

    expect(getByText('Unknown error, please try again later!')).toBeInTheDocument();
  });

  it('should display file size error notification if uploading a document fails for being too large', async () => {
    const { getByText, elements } = renderComponent();

    await waitForDomChange();

    mockDocumentService.createDocument.mockRejectedValue(new Error('[413 Error] File too large'));

    const dummyFile = new File([], 'someFile.png');
    fireEvent.change(elements.uploadButton(), { target: { files: [dummyFile] } });

    await waitForDomChange();

    expect(getByText('File too large, max limit is 10 MB')).toBeInTheDocument();
  });

  it('should display a generic error notification if uploading a document fails for unknown reasons', async () => {
    const { getByText, elements } = renderComponent();

    await waitForDomChange();

    mockDocumentService.createDocument.mockRejectedValue(new Error());

    const dummyFile = new File([], 'someFile.png');
    fireEvent.change(elements.uploadButton(), { target: { files: [dummyFile] } });

    await waitForDomChange();

    expect(getByText('Unknown error, please try again later!')).toBeInTheDocument();
  });

  it('should display an error notification if deleting a document fails', async () => {
    mockDocumentService.fetchDocuments.mockResolvedValue({ data: [{ id: '1', name: 'Document 1' }] });

    const { getByText, getAllByText } = renderComponent();

    await waitForDomChange();

    mockDocumentService.deleteDocument.mockRejectedValue(new Error());

    userEvent.click(getAllByText('Delete')[0]);

    await waitForDomChange();

    expect(getByText('Unknown error, please try again later!')).toBeInTheDocument();
  });

  it('should allow closing an error notification', async () => {
    mockDocumentService.fetchDocuments.mockRejectedValue(new Error());

    const { getByLabelText, queryByText } = renderComponent();

    await waitForDomChange();

    userEvent.click(getByLabelText('Close'));

    expect(queryByText('Unknown error, please try again later!')).not.toBeInTheDocument();
  });
});
