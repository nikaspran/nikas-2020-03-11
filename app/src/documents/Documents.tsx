import React, {
  useState, useEffect, useCallback, ChangeEvent,
} from 'react';
import { useDebounce } from 'use-debounce';
import styles from './Documents.module.css';
import DocumentCard from './components/DocumentCard';
import {
  Document,
  fetchDocuments,
  createDocument,
  deleteDocument,
} from './services/documentService';
import FileUploadButton from '../common/components/FileUploadButton';
import Input from '../common/components/Input';
import FileSize from './components/FileSize';
import ErrorNotification from '../common/components/ErrorNotification';

const GENERIC_ERROR = 'Unknown error, please try again later!';

export default function Documents() {
  const [documents, setDocuments] = useState<Document[]>([]);
  const [totalFileSize, setTotalFileSize] = useState<number>(0);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string | undefined>(undefined);
  const [uploading, setUploading] = useState(false);
  const [query, setQuery] = useState('');
  const [debouncedQuery] = useDebounce(query, 300);

  const refreshDocuments = useCallback(async () => {
    setLoading(true);
    try {
      const result = await fetchDocuments({ query: debouncedQuery });
      setDocuments(result.data);
      setTotalFileSize(result.totalFileSize);
    } catch (fetchError) {
      setError(GENERIC_ERROR);
    } finally {
      setLoading(false);
    }
  }, [debouncedQuery]);

  useEffect(() => {
    refreshDocuments();
  }, [refreshDocuments]);

  async function startUpload(files: FileList) {
    const file = files[0];
    setError(undefined);
    setUploading(true);
    try {
      await createDocument(file);
    } catch (uploadError) {
      setError(uploadError.message.includes('413') ? 'File too large, max limit is 10 MB' : GENERIC_ERROR);
    } finally {
      setUploading(false);
    }
    await refreshDocuments();
  }

  function updateQuery(event: ChangeEvent<HTMLInputElement>) {
    setError(undefined);
    setQuery(event.target.value);
  }

  async function deleteDocumentAndRefresh(document: Document) {
    setError(undefined);
    try {
      await deleteDocument(document);
    } catch (deleteError) {
      setError(GENERIC_ERROR);
      throw deleteError;
    }
    await refreshDocuments();
  }

  return (
    <div className={styles.root}>
      {error && (
        <ErrorNotification onClose={() => setError(undefined)}>
          {error}
        </ErrorNotification>
      )}

      <div className={styles.controls}>
        <Input
          placeholder="Search documents..."
          className={styles.searchBar}
          aria-label="Search documents"
          value={query}
          onChange={updateQuery}
        />

        <FileUploadButton
          className={styles.uploadButton}
          accept="image/jpeg,image/png"
          onUpload={startUpload}
          inputProps={{ 'aria-label': 'Upload document' }}
          disabled={uploading}
        >
          {uploading ? 'Uploading...' : 'Upload'}
        </FileUploadButton>
      </div>

      <div className={styles.results}>
        <div className={styles.resultStats}>
          <div className={styles.resultCount}>
            {documents.length}
            {' '}
            {documents.length === 1 ? 'document' : 'documents'}
            {loading && ' - Loading...'}
          </div>
          <div className={styles.resultSize}>Total size: <FileSize bytes={totalFileSize} /></div>
        </div>

        <div className={styles.resultItems}>
          {documents.map((document) => (
            <DocumentCard key={document.id} document={document} onDelete={deleteDocumentAndRefresh} />
          ))}
        </div>
      </div>
    </div>
  );
}
