import React from 'react';
import styles from './App.module.css';
import Documents from './documents/Documents';

function App() {
  return (
    <div className={styles.app}>
      <Documents />
    </div>
  );
}

export default App;
