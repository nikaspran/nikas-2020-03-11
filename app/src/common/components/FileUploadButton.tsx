import React, { ReactNode, ChangeEvent, InputHTMLAttributes } from 'react';
import classNames from 'classnames';
import styles from './FileUploadButton.module.css';

export default function FileUploadButton({
  children,
  className,
  multiple = false,
  accept,
  onUpload,
  inputProps = {},
  disabled,
}: {
  children: ReactNode;
  className?: string;
  multiple?: boolean;
  accept?: string;
  onUpload?: (files: FileList) => unknown;
  inputProps?: InputHTMLAttributes<HTMLInputElement>;
  disabled?: boolean;
}) {
  function notifyAboutUpload(event: ChangeEvent<HTMLInputElement>) {
    if (!onUpload || !event.target.files) {
      return;
    }

    onUpload(event.target.files);
    event.target.value = ''; // allow uploading same file multiple times (onChange won't fire on same file otherwise)
  }

  return (
    <>
      <input
        type="file"
        multiple={multiple}
        accept={accept}
        className={styles.hiddenInput}
        id="fileUpload"
        onChange={notifyAboutUpload}
        disabled={disabled}
        {...inputProps}
      />
      <label className={classNames(styles.label, className)} htmlFor="fileUpload" aria-hidden="true">
        {children}
      </label>
    </>
  );
}
