import React from 'react';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ErrorNotification from './ErrorNotification';

describe('<ErrorNotification />', () => {
  it('should render children', () => {
    const { getByText } = render(<ErrorNotification onClose={jest.fn()}>Some content</ErrorNotification>);
    expect(getByText('Some content')).toBeInTheDocument();
  });

  it('should notify when it is closed', () => {
    const onClose = jest.fn();
    const { getByLabelText } = render(<ErrorNotification onClose={onClose}>Some content</ErrorNotification>);
    userEvent.click(getByLabelText('Close'));
    expect(onClose).toHaveBeenCalled();
  });
});
