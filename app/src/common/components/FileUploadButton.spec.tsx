import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import FileUploadButton from './FileUploadButton';

const { arrayContaining } = expect;

describe('<FileUploadButton />', () => {
  it('should render children', () => {
    const { getByText } = render(<FileUploadButton>Some text</FileUploadButton>);
    expect(getByText('Some text')).toBeInTheDocument();
  });

  it('should call the onUpload handler on file upload', () => {
    const onUpload = jest.fn();
    const { getByLabelText } = render(
      <FileUploadButton onUpload={onUpload} inputProps={{ 'aria-label': 'upload' }}>Upload</FileUploadButton>,
    );

    const dummyFile = new File([], 'someFile.png');
    fireEvent.change(getByLabelText('upload'), { target: { files: [dummyFile] } });

    expect(onUpload).toHaveBeenCalledWith(arrayContaining([
      dummyFile,
    ]));
  });
});
