import React, { InputHTMLAttributes } from 'react';
import classNames from 'classnames';
import styles from './Input.module.css';

export default function Input({
  className,
  ...otherProps
}: InputHTMLAttributes<HTMLInputElement>) {
  return (
    <input className={classNames(styles.input, className)} {...otherProps} />
  );
}
