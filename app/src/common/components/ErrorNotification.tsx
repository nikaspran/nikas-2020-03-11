import React, { ReactNode } from 'react';
import styles from './ErrorNotification.module.css';

export default function ErrorNotification({
  children,
  onClose,
}: {
  children: ReactNode;
  onClose: () => unknown;
}) {
  return (
    <div className={styles.notification}>
      <button type="button" className={styles.closeButton} onClick={onClose} aria-label="Close">×</button>
      {children}
    </div>
  );
}
